<?php

namespace Blog;
use Nette;

class ArticleRepository extends Repository{

	public function findArticles(){
		return $this->findAll()->order('created DESC');
	}

	public function findLast(){
		return $this->findArticles()->limit(1)->fetch();
	}

	public function createArticle($author, $title, $text){
		$date = new \DateTime();
		return $this->getTable()->insert(array(
			'author' => $author,
			'title' => $title,
			'text' => $text,
			'created'=>$date->format("Y-m-d H:i:s")
			));
	}

	public function editArticle($id,$title,$text){
		$date = new \DateTime();
		return $this->getTable()->where('id',$id)->update(array(
			'title' => $title,
			'text' => $text,
			'edited' => $date->format("Y-m-d H:i:s")
			));
	}
	
}