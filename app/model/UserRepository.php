<?php

namespace Blog;
use Nette;

class UserRepository extends Repository{
	
	public function findByName($username)
	{
	    return $this->findAll()->where('username', $username)->fetch();
	}
	
	public function createUser($auth,$values){
		/* tady, nebo v presenteru? */
		$salt = $this->getSalt();
		$password = $auth->calculateHash($values->password,$salt);

		return $this->getTable()->insert(array(
				'username'=>$values->username,
				'name'=>$values->name,
				'password'=>$password,
				'salt'=>$salt
			));
	}

	protected function getSalt(){
		return md5(uniqid(null, true));
	}
}