<?php

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{

	private $articleRepository;
	private $article;

	private $userRepository;

	public function startup(){
		parent::startup();
		$this->articleRepository = $this->context->articleRepository;
		$this->userRepository = $this->context->userRepository;
	}

	public function actionDefault($id){

		if(!empty($id)){
			$this->article = $this->articleRepository->findBy(array('id'=>$id))->fetch();
			if($this->article === false)$this->setView('notFound');
		}
		else $this->article = $this->articleRepository->findLast();
	}
  
	public function renderDefault($id) //action default? + pridat presmerovani na notFound
	{
		$this->template->articles = $this->articleRepository->findArticles();
		if($this->article)
			$this->template->author = $this->userRepository->findBy(array('id'=>$this->article->author))->fetch();
		$this->template->chosen = $this->article;
	}

}
