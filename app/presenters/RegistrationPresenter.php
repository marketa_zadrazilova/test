<?php 

use Nette\Application\UI\Form;

class RegistrationPresenter extends BasePresenter{

	private $userRepository;
	private $authenticator;

	public function startup(){
		parent::startup();
		$this->userRepository = $this->context->userRepository;
		$this->authenticator = $this->context->authenticator;
	}

	public function actionDefault(){

	}

	public function renderDefault(){

	}

	protected function createComponentRegistrationForm(){
		$form = new Form();
		$form->addText('username', 'Uživatelské jméno:', 40, 50)->setRequired('Zadejte uživatelské jméno.');
		$form->addText('name','Jméno:', 40, 100)->setRequired('Zadejte vaše jméno.');
		$form->addPassword('password', 'Heslo:', 20, 20)->setRequired('Zadejte heslo.');
		$form->addPassword('password2', 'Zopakujte heslo:', 20, 20)->setRequired('Zopakujte heslo.')->addRule(Form::EQUAL, 'Hesla se neshodují.', $form['password']);
		$form->addSubmit('register', 'Registrovat');

		$form->onSuccess[] = $this->registrationFormSubmitted;
		return $form;
	}

	public function registrationFormSubmitted($form){
		if($this->userRepository->findByName($form->values->username)){
			$form->addError('Uživatelské jméno je již obsazeno.');
		}else{
			$this->userRepository->createUser($this->authenticator,$form->values);
			$this->flashMessage('Registrace proběhla úspěšně.', 'success');
			$this->redirect('Sign:in');
		}
	}	
	

}