<?php

use Nette\Application\UI;


/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{


	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		$form = new UI\Form;
		$form->addText('username', 'Uživatelské jméno:');
		$form->addPassword('password', 'Heslo:');
		$form->addCheckbox('remember', 'Pamatovat si mě na tomto počítači');
		$form->addSubmit('login', 'Přihlásit');

		// call method signInFormSucceeded() on success
		$form->onSuccess[] = $this->signInFormSucceeded;
		return $form;
	}


	public function signInFormSucceeded($form)
	{
	    try {
	        $user = $this->getUser();
	        $values = $form->getValues();
	        if ($values->remember) {
	            $user->setExpiration('+30 days', FALSE);
	        }
	        $user->login($values->username, $values->password);
	        $this->flashMessage('Přihlášení bylo úspěšné.', 'success');
	        $this->redirect('Homepage:');
	    } catch (Nette\Security\AuthenticationException $e) {
	        $form->addError('Neplatné uživatelské jméno nebo heslo.');
	    }
	}


	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Odhlášení proběhlo úspěšně.','success');
		$this->redirect('in');
	}

}
