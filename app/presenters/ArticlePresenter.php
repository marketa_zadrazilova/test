<?php

use Nette\Application\UI\Form;

class ArticlePresenter extends BasePresenter{
	
	private $articleRepository;
	private $article;

	protected function startup(){
		parent::startup();

		if(!$this->getUser()->isLoggedIn()){
			$this->redirect('Sign:in');
		}

		$this->articleRepository = $this->context->articleRepository;
	}

	public function actionDefault($id){

	}

	public function renderDefault($id){

	}

	protected function createComponentNewArticleForm(){

		$form = new Form();
		$form->addText('title', 'Titulek: ', 50, 50)->setRequired('Zadejte prosím titulek článku.');
		$form->addTextArea('text', 'Text článku:')->setRequired('Zadejte prosím text článku.');
		$form->addSubmit('create', 'Vytvořit');
		$form->onSuccess[] = $this->newArticleFormSubmitted;

		return $form;
	}

	public function newArticleFormSubmitted($form){
		$user = $this->getUser()->id;
		$this->articleRepository->createArticle($user,$form->values->title, $form->values->text);
		$this->flashMessage('Článek byl vytvořen.','success');
		$this->redirect('Homepage:');
	}

	public function actionEdit($id){
		$this->article = $this->articleRepository->findBy(array('id'=>$id))->fetch();

		if($this->getUser()->id != $this->article->author){
			$this->redirect('Homepage:',$id);
		}
	}

	protected function createComponentEditArticleForm($id){
		$form = new Form();
		$form->addText('title', 'Titulek: ', 50, 50)->setDefaultValue($this->article->title)->setRequired('Zadejte prosím titulek článku.');
		$form->addTextArea('text', 'Text článku:')->setDefaultValue($this->article->text)->setRequired('Zadejte prosím text článku.');
		$form->addSubmit('create', 'Uložit');

		$form->onSuccess[] = $this->editArticleFormSubmitted;

		return $form;
	}

	public function editArticleFormSubmitted($form){
		$this->articleRepository->editArticle($this->article->id,$form->values->title, $form->values->text);
		$this->flashMessage('Změny byly uloženy', 'success');
		$this->redirect('Homepage:',$this->article->id);
	}

	public function renderEdit($id){

	}
}